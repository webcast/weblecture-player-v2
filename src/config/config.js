//  This is the config file to implement paella player on cern's network

const dev = {};

const prod = {};

const test = {};
// https://wowza.cern.ch/livehd/smil:863086_camera_all.smil/playlist.m3u8
// https://wowza.cern.ch/livehd/smil:863086_slides_all.smil/playlist.m3u8
let tempConfig = dev;

if (process.env.NODE_ENV === "production") {
  tempConfig = prod;
}

if (process.env.NODE_ENV === "test") {
  tempConfig = test;
}
const config = tempConfig;

export default config;
