export const PACKAGE_VERSION = "2.7.0";
export const PAELLA_CORE_VERSION = "1.46.0";
export const PAELLA_BASIC_PLUGINS_VERSION = "1.44.10";
