export interface IManifestMetadata {
  preview: string;
}
export interface IManifest {
  metadata: IManifestMetadata;
}
