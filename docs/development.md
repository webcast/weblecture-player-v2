# Development

## Requirements

- Node v16.15.x
- Yarn 1.22.x

## Technologies

- React

## Local install

### Dependencies

Install the project dependencies
```
npm install
```

## Run

### Webapp

```bash
npm start
```

## Links to libs and other docs

- Paas Docs: https://paas.docs.cern.ch/