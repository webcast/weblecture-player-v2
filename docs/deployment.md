# Deployment

> ⚠️ Secrets and service accounts are not restored. You need to create them manually. These include:
> - Secret `gitlab-registry-auth`: To access the Gitlab Registry
> - Service Account `gitlabci-deployer`: To give Gitlab access to deploy in Openshift

## Add the Openshift deploy key to Gitlab

> To grant access to the private Gitlab repository

```
oc login paas.cern.ch
oc project (ttaas-test|ttaas-qa|ttaas) # 2 options
oc get secrets/sshdeploykey -o go-template='{{index .metadata.annotations "cern.ch/ssh-public-key"}}{{println}}'
```

## Deploy a Docker image in OKD4

### 1. Generate the deploy token for Gitlab CI

1. Run the following commands in Openshift

```bash
oc create serviceaccount gitlabci-deployer
```

```bash
oc policy add-role-to-user edit -z gitlabci-deployer
```

Create a Secret in Openshift and extract the token from the CLI:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: gitlabci-deployer-token
  annotations:
    kubernetes.io/service-account.name: gitlabci-deployer
type: kubernetes.io/service-account-token
```

```bash
oc get secret gitlabci-deployer-token -o jsonpath='{.data.token}' | base64 -d
```

1. Paste the generated token in Gitlab `Settings/CI/CD/Variables` as `IMAGE_IMPORT_TOKEN_<PROD|QA|TEST>`.


### 2. Give access OKD4 to the registry

1. In registry.cern.ch create a robot account for the project and copy the username and the password.
2. In OKD4, create a pull image secret called `registry.cern.ch` with the following content:
   1. URL: https://registry.cern.ch
   2. Username: The robot account username
   3. Password: The robot account password
3. Go to service accounts and add the recently created `registry.cern.ch` pull secret to the user `default`.


## Restore the QA application (deployment configs, image streams, services and routes)

To restore a backup of the elements in Openshift:


```bash
oc create -f okd4/service.yaml
```
```bash
oc create -f okd4/route.yaml
```
```bash
oc create -f okd4/deployment.yaml
```

- By default, the ImageStream will take the `qa` Docker image, go to the ImageStream and change `qa` with `prod`.
- The route will point to the test app. Change this to the hostname of your application.

## OKD4 useful commands

```bash
oc debug <POD_NAME>
```