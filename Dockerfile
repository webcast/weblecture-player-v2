FROM nginx:1.24

EXPOSE 8080

RUN mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf_old
RUN mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf_old

ADD nginx/nginx.conf /etc/nginx/nginx.conf
ADD nginx/webapp.conf /etc/nginx/conf.d/default.conf

COPY build/ /usr/share/nginx/html

RUN chown -R 0 /usr/share/nginx/html && chmod -R 755 /usr/share/nginx/html && \
  chown -R 0 /var/cache/nginx && \
  chown -R 0 /var/log/nginx && \
  chown -R 0 /etc/nginx/conf.d

RUN touch /var/run/nginx.pid && \
  chown -R 0 /var/run/nginx.pid

USER 1001
